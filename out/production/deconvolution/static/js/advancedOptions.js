/**
 * Created by maaike on 10-3-17.
 * This javascript handles the advanced options. Yet to be implemented.
 */

function more(obj) {
    var content = document.getElementById("showMore");

    if (content.style.display == "none") {
        content.style.display = "";
        obj.innerHTML = "-";
    } else {
        content.style.display = "none";
        obj.innerHTML = "+";
    }
}
