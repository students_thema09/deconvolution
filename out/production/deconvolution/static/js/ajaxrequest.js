/**
 * Created by maaike on 24-3-17.
 */
$(document).ready( function () {
    $('#ResultsTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/showData.do",
        "columns": [
            {"data": "genotypeName"},
            {"data": "pValues.0"},
            {"data": "pValues.1"},
            {"data": "pValues.2"},
            {"data": "pValues.3"}
        ]
    } );
} );
