# README #

### What is this repository for? ###

* Deconvolution project for theme 11 and theme 12
* Client: UMCG Niek de Klein and Raul Aguirre-Gamboa
* Hanze Hogeschool: Michiel Noback

### How do I get set up? ###

* Download the zip from the bitbucket download section
* Extract the zip
* Execute the start.sh or start.bat file (dependent of your operating system)  
* The program is running now, the default webbrowser will automatically open and the site will be loaded

### Who do I talk to? ###

* Maaike Brummer (m.l.brummer@st.hanze.nl)
* Iris Gorter (i.m.gorter@st.hanze.nl)