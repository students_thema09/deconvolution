/**
 * Created by mlbrummer on 8-5-17.
 */

/**
 * Method to link to different tabs in the results page
 * @param ResultType
 */
function openTab( ResultType) {
    var i;
    var x = document.getElementsByClassName("tabcontent");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(ResultType).style.display = "block";
}


