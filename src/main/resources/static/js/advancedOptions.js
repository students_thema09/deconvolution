/**
 * This method handles the (dis)appearing of the 2 forms. When the first form is visible, the second is not, and vice versa.
 */
function test() {
    $("#first").hide();
    if (document.getElementById('form').checked) {
        $("#second").show();
    } else if (document.getElementById('result').checked){
        window.location.href = "http://localhost:8080/result";
    } else {
        $("#first").show();
    }
}

/**
 * This method is called when you link to the result page when the data is still being processed. The spinner will appear.
 */
function resultSpinner() {
    console.log("Spinner result");
    var opts = {
        lines: 13, // The number of lines to draw
        length: 56, // The length of each line
        width: 14, // The line thickness
        radius: 78, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        color: '#000', // #rgb or #rrggbb
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '200', // Top position relative to parent in px
        left: '500', // Left position relative to parent in px
        visibility: true
    };

    var el = document.getElementById("spinnerContainerResult");
    if (el) { el.style.display = (el.style.display === 'none') ? 'block' : 'none'; }

    $('#spinnerContainerResult').after(new Spinner(opts).spin().el);
}


/**
 * When the form document is ready, the tooltips are visible and when the submit button is clicked, there will be a spinner.
 */
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();

    $('#submitForm').submit(function () {
        console.log("submitted!");
        var opts = {
            lines: 13, // The number of lines to draw
            length: 56, // The length of each line
            width: 14, // The line thickness
            radius: 78, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            color: '#000', // #rgb or #rrggbb
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '200', // Top position relative to parent in px
            left: '500', // Left position relative to parent in px
            visibility: true
        };

        var el = document.getElementById("spinnerContainer");
        if (el) { el.style.display = (el.style.display === 'none') ? 'block' : 'none'; }

        $('#spinnerContainer').after(new Spinner(opts).spin().el);
    });
});

/**
 * This method handles the extra input field when the param -m is checked
 * @param cbox
 */
function getParamInputAmount(cbox) {
    if (cbox.checked) {
        var input = document.createElement("input");
        input.type = "number";
        var div = document.createElement("div");
        div.id = cbox.name;
        input.id = cbox.id;
        input.name = cbox.id;
        div.innerHTML = "Minimum amount for " + cbox.name;
        div.appendChild(input);
        document.getElementById("insertinputs").appendChild(div);
    } else {
        document.getElementById(cbox.name).remove();
    }
}
