/**
 * Method to get the largest value of an array
 * @param array
 * @returns largest value of an array
 */
function largest ( array ) {
    return Math.max.apply( Math, array );
};

/**
 * Method to create circles of different sizes in a canvas.
 * @param perc array of percentages, how many times is the cell available
 * @param columnarray array with the names of the cells of the percentages
 */
function drawCircle(perc, columnarray) {

    if (perc.length < columnarray.length) {
        //spearman correlation available, delete it.
        columnarray.pop();

    }
    var ctx = document.getElementById('circle').getContext('2d');

    var biggestRadius = (largest(perc)*3/2) + 20;

    var column = 0;
    var row = 1;
    var x = biggestRadius;
    var y = biggestRadius * row;
    var totalInARow = 4;
    var yFirstRadius = (perc[0]*3)/2;
    var font = "bold " + 30 +"px serif";

    for (var i = 0; i < perc.length; i++) {
        // For every percentage in the list of percentages
        if (x + ((perc[i]*3)/2) > 1150) {
            // If the x coordinate will be out of the canvas, create new row
            console.log("x + ((perc[i]*3)/2) > 1150 --- " + x + ((perc[i]*3)/2));
            row++;
            column = 0;
            totalInARow = i;
            x = biggestRadius;
            y = (biggestRadius * 2) * row;
            yFirstRadius = (perc[i]*3)/2;
        } else {
            console.log("x + ((perc[i]*3)/2) < 1150 --- " + x + ((perc[i]*3)/2))
        }

        // Set the design of the canvas objects
        ctx.beginPath();
        ctx.arc(x, y, (perc[i]*3)/2, 0, Math.PI * 2, false);
        ctx.lineWidth = 3;
        ctx.strokeStyle = '#FF0000';
        ctx.fillStyle = '#FFFBFF';
        ctx.closePath();
        ctx.stroke();
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle = '#000100';
        ctx.font = font;
        ctx.textAlign = 'center';
        ctx.fillText(columnarray[i].replace('"', '').replace('"', ''), x, y + yFirstRadius + 30);
        ctx.fill();

        column++;

        console.log("totalInARow: " + totalInARow);
        if (column > totalInARow-1) {
            // If the columnnumber will be higher than the max of columns defined on the basis of the width of the canvas, create new row
            column = 0;
            row++;
            x = biggestRadius;
            y = (biggestRadius * 2) * row;
            yFirstRadius = (perc[i]*3)/2;
        } else {
            //Else, just add the biggest radius *2 to the x value
            x = x + biggestRadius*2;
        }

    }

}
