/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution.deconvolution.server;

import com.sun.glass.ui.Window;
import deconvolution.DeconvolutionApplication;
import deconvolution.deconvolution.control.HomeController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles the request of sending messages. It is also used to start the external server correctly by
 * defining the servername and port
 */
@Component
public class MessageClient {

    private static HomeController homeController;

    @Autowired
    public MessageClient(HomeController homeController) {
        this.homeController = homeController;
    }

    private static DataOutputStream out;
    private static Socket client;

    /**
     * This method creates the connection with the server.
     */
    public static void createServerConnection() {
        String serverName = "localhost";
        int port = 8081;

        try {
            Logger.getLogger("MessageClient").log(Level.INFO, "[Client] Connecting to " + serverName + " on port " + port);
            client = new Socket(serverName, port);

            Logger.getLogger("MessageClient").log(Level.INFO, "[Client] Just connected to " + client.getRemoteSocketAddress());
            OutputStream outToServer = client.getOutputStream();
            out = new DataOutputStream(outToServer);

            out.writeUTF("[Client] Hello from " + client.getLocalSocketAddress());
            out.writeUTF("[Client] " + client.getLocalSocketAddress());

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * This method sends a message to the external server to start the deconvolution jar with the temporary file
     * locations and other parameters
     * @param cellCountFile
     * @param expressionlevelFile
     * @param genotypeDosageFile
     * @param paramW
     * @param tmpdir
     */
    public static void sendMessage(File cellCountFile, File expressionlevelFile, File genotypeDosageFile, File snpsToTestFile, String paramAd, String paramM, String paramNn, String paramW, String tmpdir) throws FileNotFoundException {
        if (!cellCountFile.exists() || !expressionlevelFile.exists() || !genotypeDosageFile.exists() || !snpsToTestFile.exists()) {
            throw new FileNotFoundException("File not found");
        }
        if (cellCountFile.length() == 0 || expressionlevelFile.length() == 0 || genotypeDosageFile.length() == 0 || snpsToTestFile.length() == 0) {
            throw new FileNotFoundException("File is empty");
        }

        //Set all the params for the deconvolution program
        String paramad = "";
        String paramm = "";
        String paramnn = "";
        String paramw = "";
        String params = "";

        if (paramAd != null) {
            paramad = " -ad " + paramAd;
        } else {
            paramad = "";
        }

        if (paramM != null) {
            paramm = " -m " + paramM;
        } else {
            paramm = "";
        }

        if (paramNn != null) {
            paramnn = " -nn " + paramNn;
        } else {
            paramnn = "";
        }

        if (paramW != null) {
            paramw = " -w";
        } else {
            paramw = "";
        }
        params = paramad + paramm + paramnn + paramw;

        //Create the path to the /Tool in the zip file
        final File f = new File(DeconvolutionApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String path = f.toString().split("Tool")[0] + "/Tool/";

        //Create the statement that has to be executed in the external server.
        String deconjar = "java8 -jar " + path + "Deconvolution/deconvolution.jar -o " + tmpdir + "/" + " -c " + cellCountFile + " -e " + expressionlevelFile + " -g " + genotypeDosageFile + " -sn " + snpsToTestFile + " -f" + params;

        try {
            //Send the statement to the server
            out.writeUTF(deconjar);

            homeController.flagProcessingStarted = true;
            Logger.getLogger("MessageClient").log(Level.INFO, "flasProcessingStarted is set to TRUE");

            //Get the output
            InputStream inFromServer = client.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);

            Logger.getLogger("MessageClient").log(Level.INFO, "[Client] Server says " + in.readUTF());
            client.close();

        } catch (IOException e) {
            homeController.flagProcessingStarted = false;
            Logger.getLogger("MessageClient").log(Level.INFO, "flagProcessingStarted is set to FALSE");
            e.printStackTrace();
        }

    }
}
