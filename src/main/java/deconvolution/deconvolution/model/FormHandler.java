/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution.deconvolution.model;

/**
 * This class handles the variable input of the form
 * Contains getters and setters
 */
public class FormHandler {
    private String celltype;
    private String paramW;

    public FormHandler() {
    }

    public String getCelltype() {
        return celltype;
    }

    public void setCelltype(String celltype) {this.celltype = celltype;}

    public String getParamW() {
        return paramW;
    }

    public void setParamW(String paramW) {
        this.paramW = paramW;
    }
}