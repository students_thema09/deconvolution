package deconvolution.deconvolution.model;

/**
 * This class contains multiple pValues per Genotype
 * This makes it possible to make multiple genotypes with their own pvalues and name.
 */
public class Genotype {
    private float[] pValues;
    private String genotypeName;
    private int currentCellIndex = 0;
    private int nCellTypes = ColumnDefiner.getCelltypes().size();

    public Genotype(float[] pValues) {
        this.pValues = pValues;
    }

    public Genotype() {
        this.pValues = new float[this.nCellTypes];
    }

    public float getPvalue(CellType cellType) {
        return this.pValues[cellType.ordinal()];
    }

    public float[] getpValues() {
        return pValues;
    }

    public void addPvalue(String cellType, float pvalue) {
        this.pValues[this.currentCellIndex] = pvalue;
        this.currentCellIndex++;
    }
    public void setGenotypeName(String genotypeName) {
        this.genotypeName = genotypeName;
    }
    public String getGenotypeName() {
        return genotypeName;
    }

}
