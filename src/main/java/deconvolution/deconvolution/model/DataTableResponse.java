package deconvolution.deconvolution.model;

import java.util.List;

/**
 * This class controls the response of the Datatable
 * for example when the table needs to be filtered
 */
public class DataTableResponse {

    private int draw;
    private int recordsTotal;
    private int recordsFiltered;
    private List<Genotype> data;
    private List<Snp> dataS;


    public int getDraw() {return draw;}

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<Genotype> getData() {
        return data;
    }

    public void setData(List<Genotype> data) {
        this.data = data;
    }
    public void setDataSNP(List<Snp> data) {this.dataS = data;}

}
