package deconvolution.deconvolution.model;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Author imgorter
 * TODO add comments and javadoc
 */
public class ColumnDefiner {
    public static List<String> celltypes;
    public static String spearman;

    public static List<String> getCelltypes() {
        return celltypes;
    }

    public void setCelltypes(List<String> celltypes) {
        this.celltypes = celltypes;
    }

    public void setSpearman(String spearman) {
        this.spearman = spearman;
    }

    public static String getSpearman() {
        return spearman;
    }

    /**
     * This method retrieves the celltypes out of the uploaded cellcount file.
     * The celltypes are the first line of the cellcount file and are seperated by tab.
     * Sometimes the line starts with a tab. This tab should not be saved.
     * @param cellcount_tmp
     * @param paramW
     * @return List Celltypes
     */
    public void DefineCellTypes(File cellcount_tmp, String paramW){
        MeanCalculator mc = new MeanCalculator();
        try {
            BufferedReader br = new BufferedReader(new FileReader(cellcount_tmp));
            String line = br.readLine();
            List<String> columns= new ArrayList<>();

            //Only if paramw is present, call define spearmanCorrelation
            String param = "";
            if (paramW != null && !paramW.isEmpty()) {
                Logger.getLogger("ColumnDefiner").log(Level.INFO, "Param w is present" );
                DefineSpearmanCorrelation();
                param = getSpearman();
            } else {
                Logger.getLogger("ColumnDefiner").log(Level.INFO, "Param w is not present" );
            }

            if(line.startsWith("\t")){
                String newline = line.substring(1);
                String[] splittedline = newline.split("\t");
                for (String t : splittedline) {
                    if (t.contains(" ")) {
                        columns.add(t.replace(" ", "_"));
                    } else {
                        columns.add(t);
                    }
                }
                if (!param.equals("")) {
                    columns.add(param);
                }
                setCelltypes(columns);

            }
            else {
                String[] splittedline = line.split("\t");
                for (String r : splittedline) {
                    if (r.contains(" ")) {
                        columns.add(r.replace(" ", "_"));
                    } else {
                        columns.add(r);
                    }
                }
                if (!param.equals("")) {
                    columns.add(param);
                }
                setCelltypes(columns);
                mc.CalculateMean(cellcount_tmp, columns);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that gets the name of the last column in the output file.
     * replaces the spaces in the column name with a _.
     */
    public void DefineSpearmanCorrelation(){
        String tmpdir = System.getProperty("java.io.tmpdir");
        try {
            BufferedReader br = new BufferedReader(new FileReader(tmpdir + "/deconvolutionResults.csv"));
            String line = br.readLine();

            if(line.startsWith("\t")){
                String newline = line.substring(1);
                String[] splittedline = newline.split("\t");
                String col = splittedline[splittedline.length-1];
                String[] colSplit = col.split(" ");
                col = colSplit[0] + "_" + colSplit[1];
                setSpearman(col);
            }
            else{
                String[] splittedline = line.split("\t");
                String col = splittedline[splittedline.length-1];
                String[] colSplit = col.split(" ");
                col = colSplit[0] + "_" + colSplit[1];
                setSpearman(col);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
