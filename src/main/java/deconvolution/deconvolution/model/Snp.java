package deconvolution.deconvolution.model;

import java.util.List;

/**
 * This class contains multiple celltypes per Snp
 * This makes it possible to make multiple snps with their own celltypes and name.
 */
public class Snp {

    private String[] celltypes;
    private String snpName;
    private int currentCellIndex = 0;
    private int nCellTypes = getCellcount();

    public Snp(String[] celltypes) {
        this.celltypes = celltypes;
    }

    /**
     * This method returns the number of cells
     * @return int cellcount
     */
    public int getCellcount() {
        int cellcount = 0;
        List<String> cells = ColumnDefiner.getCelltypes();
        if (cells.contains("Spearman_correlation")) {
            cellcount = cells.size() -1;
        } else {
            cellcount = cells.size();
        }
        return cellcount;
    }

    public Snp() {
        this.celltypes = new String[this.nCellTypes];}

    public String getSnpName() {
        return snpName;
    }

    public void setSnpName(String snpName) {
        this.snpName = snpName;
    }

    /**
     * This method adds a celltype to the snp
     * @param celltype
     */
    public void addCelltype(String celltype) {
        this.celltypes[this.currentCellIndex] = celltype;
        this.currentCellIndex++;
    }

    public String[] getCelltypes() {
        return celltypes;
    }

    @Override
    public String toString() {
        return "Snp{" +
                "snpName='" + snpName + '\'' +
                ", celltypes='" + celltypes + '\'' +
                '}';
    }
}
