package deconvolution.deconvolution.model;

/**
 * This enum contains all the celltypes
 *
 */
public enum CellType {
    NEUTROPHILE,
    LYMPHOCYTE,
    MONOCYTE,
    EOSINOPHILE;


    public static CellType valueOfCaseInsensitive(String name) {
        return CellType.valueOf(name.toUpperCase());
    }

    /**
     * This class is used as an exception when a empty file is encountered
     */
    public static class EmptyFileException extends Exception {
        public EmptyFileException() {}

        public EmptyFileException(String message)
        {
            super(message);
        }
    }
}
