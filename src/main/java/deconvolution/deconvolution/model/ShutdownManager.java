package deconvolution.deconvolution.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;

/**
 * Class to shut down the program
 */
@Component
public class ShutdownManager {
    @Autowired
    private ApplicationContext appContext;

    /**
     * Method that shuts down the springapplication and gives an returnCode
     * @param returnCode
     */
    public void initiateShutdown(int returnCode){
        SpringApplication.exit(appContext, () -> returnCode);
    }
}
