package deconvolution.deconvolution.model;

import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class sets attributes and tablinks
 */
public class SessionHandler {

    /**
     * This method sets all the session attributes for the html pages.
     * @param session
     */
    public static void setSessionAttributes(HttpSession session) {
        String columns = "GenomeName " + ColumnDefiner.getCelltypes();
        session.setAttribute("columns", columns);

        if (columns.contains("Spearman_correlation")) {
            session.setAttribute("columnsSNP", columns.replace(", Spearman_correlation", ""));
        } else {
            session.setAttribute("columnsSNP", columns);
        }

        int amountOfCells = columns.replace("[", "").replace("]", "").split(",").length;
        if (columns.contains("Spearman_correlation")) {
            session.setAttribute("cells", amountOfCells - 1);
        } else {
            session.setAttribute("cells", amountOfCells);
        }

        session.setAttribute("percentages", MeanCalculator.getPercentages());
        //Set the height of the canvas, dependent of the amount of percentages and sizes.
        session.setAttribute("height", MeanCalculator.getPercentages().size()/3*550);

    }

    /**
     * This method handles the redirection to the tablink.
     * @param session
     * @param tab
     */
    public static void setTabSession(HttpSession session, String tab) {
        if (tab.equals("SNP")) {
            session.setAttribute("tab", "SNP");
        } else if (tab.equals("Table")) {
            session.setAttribute("tab", "Table");
        } else if (tab.equals("Bloodcells")) {
            session.setAttribute("tab", "Bloodcells");
        } else {
            Logger.getLogger("SessionHandler").log(Level.INFO, "No tab string");
        }
    }
}
