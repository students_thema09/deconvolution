package deconvolution.deconvolution.model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class calculates the mean of every value from a column in de uploaded cellcounts file.
 * This percentage is used for the scaling of the circles that reflects the cells.
 */
public class MeanCalculator {
    List<Float> columnTotal = new ArrayList<>();
    static List<Float> percentages = new ArrayList<>();

    /**
     * This method gets the list of percentages
     * @return
     */
    public static List<Float> getPercentages() {
        return percentages;
    }

    /**
     * This method sets the list percentages
     * @param percentages
     */
    public void setPercentages(List<Float> percentages) {
        this.percentages = percentages;
    }

    public void CalculateMean(File Cellcounts, List columns) throws IOException {
        BufferedReader br = null;
        String sCurrentLine;

        br = new BufferedReader(new FileReader(Cellcounts));

        String firstLine = br.readLine();
        int ColumnLength = firstLine.split("\t").length;
        int nLines = 0;


        List[] dataForPlot = {columns, percentages};

        for (int i = 0; i < ColumnLength; i++) {
            columnTotal.add(Float.valueOf(0));
        }

        while ((sCurrentLine = br.readLine()) != null) {
            nLines++;
            String[] arr = sCurrentLine.split("\t");
            for (int i = 1; i < arr.length; i++) {
                columnTotal.set(i - 1, (columnTotal.get(i - 1) + Float.valueOf(arr[i])));
            }
        }

        for (float percentage : columnTotal) {
            percentages.add(percentage / nLines);

        }
        setPercentages(percentages);
    }

}
