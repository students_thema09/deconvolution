/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution.deconvolution.model;

import deconvolution.deconvolution.server.MessageClient;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class copy's the content of the uploaded files to a temporary file on the tmp location of the computer and sends
 * a message to the external server
 */
public class FileCopyist {
    /**
     * This method copy's the content of uploaded files to tmp files and sends a message to the external server to start
     * the deconvolution program with the temporary files
     * @param cellCountFile
     * @param expressionLevelFile
     * @param genotypeDosageFile
     * @throws IOException
     */
    public static void copyFiles(MultipartFile cellCountFile, MultipartFile expressionLevelFile, MultipartFile genotypeDosageFile, MultipartFile snpsToTestFile, String paramAd, String paramM, String paramNn, String paramW) throws IOException {
        File cellCountFile_tmp = null;
        File expressionLevelFile_tmp = null;
        File genotypeDosageFile_tmp = null;
        File snps_tmp = null;
        String cellcount_prefix = "cellcount_tmp";
        String expressionlevel_prefix = "expressionlevel_tmp";
        String genotypedosage_prefix = "genotypedosage_tmp";
        String snps_prefix = "snps_tmp";
        String suffix = ".txt";
        String tmpdir = System.getProperty("java.io.tmpdir");

        String cellcountFileName = (cellcount_prefix + suffix) ;
        cellCountFile_tmp = new File(tmpdir, cellcountFileName);
        FileOutputStream fosCellCount = new FileOutputStream(cellCountFile_tmp);
        fosCellCount.write(cellCountFile.getBytes());
        fosCellCount.close();

        String expressionFileName = (expressionlevel_prefix + suffix);
        expressionLevelFile_tmp = new File(tmpdir, expressionFileName);
        FileOutputStream fosExpression = new FileOutputStream(expressionLevelFile_tmp);
        fosExpression.write(expressionLevelFile.getBytes());
        fosExpression.close();

        String genotypeFileName = (genotypedosage_prefix + suffix);
        genotypeDosageFile_tmp = new File(tmpdir, genotypeFileName);
        FileOutputStream fosGenotype = new FileOutputStream(genotypeDosageFile_tmp);
        fosGenotype.write(genotypeDosageFile.getBytes());
        fosGenotype.close();

        String snpsFileName = (snps_prefix + suffix);
        snps_tmp = new File(tmpdir, snpsFileName);
        FileOutputStream fosSnps = new FileOutputStream(snps_tmp);
        fosSnps.write(snpsToTestFile.getBytes());
        fosSnps.close();

        //send the message to start the deconvolution program
        MessageClient.sendMessage(cellCountFile_tmp, expressionLevelFile_tmp, genotypeDosageFile_tmp, snps_tmp,  paramAd, paramM, paramNn, paramW, tmpdir);
        //Define the celltypes
        ColumnDefiner columnDefiner = new ColumnDefiner();
        columnDefiner.DefineCellTypes(cellCountFile_tmp, paramW);

    }
}
