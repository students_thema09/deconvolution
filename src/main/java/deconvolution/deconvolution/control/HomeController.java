/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution.deconvolution.control;

import deconvolution.deconvolution.database.sqlite.SqliteConnector;
import deconvolution.deconvolution.model.*;
import deconvolution.deconvolution.server.MessageClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


@Controller
@Component
/**
 * This class controls the directing of the html pages.
 * It makes the index page available on multiple urls.
 * It also handles the entered values in the form page
 * It makes it possible to reinitialize the web page
 * It also handles the loading symbol after pressing submit
 *
 */
public class HomeController {
    @Autowired
    private SqliteConnector sqliteConnector;

    @Autowired
    public ShutdownManager shutdownManager;

    boolean flagProcessingDone = false;
    public boolean flagProcessingStarted = false;

    @RequestMapping(value = {"", "/", "/index", "/index.html", "/home", "/home.html", "/greeting", "/greeting.html"})
    /**
     * This method redirects to the home page when one of above urls is entered.
     * @return String page greeting
     */
    public String greeting(@RequestParam(
            value = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/form")
    /**
     * This method redirects to the page that displays a form when /form is entered or a button is pressed
     * @return String form
     */
    public String form(Model model, HttpSession session) {
        model.addAttribute("form", new FormHandler());
        sqliteConnector.setFromPage("form");
        //Set the attribute noResultsAvailable on 0, so that the html does not show the warning.
        session.setAttribute("noResultsAvailable", 0);
        return "form";
    }

    @GetMapping("/reinitialize")
    /**
     * This method redirects to the page that displays a form when /reinitialize is entered.
     * It sets the fromPage on reinitialize so that the formSubmit method in the HomeController knows
     * that the data has to be deleted.
     */
    public String reinitialize(Model model, HttpSession session) {
        model.addAttribute("form", new FormHandler());
        sqliteConnector.setFromPage("reinitialize");
        //Set the attribute noResultsAvailable on 0, so that the html does not show the warning.
        session.setAttribute("noResultsAvailable", 0);
        return "form";
    }

    @GetMapping("/result")
    /**
     * This method redirects to the page that displays the results when /result is entered.
     * It checks if the processing is started and if the processing is finished. It shows a spinner if the data is still processing.
     * @param model
     * @param session
     * @return String page.
     */
    public String result(Model model, HttpSession session) {
        SessionHandler.setSessionAttributes(session);

        if (flagProcessingStarted) {
            //Processing is started

            //Set attribute addSpinner on 1, spinner is in the html page.
            session.setAttribute("addSpinner", 1);

            try {
                while (!flagProcessingDone) {
                    //Not done
                    Logger.getLogger("HomeController").log(Level.INFO, "Processing is not done yet: " + new Date());

                    //Wait 60 seconds and try again.
                    Thread.sleep(60 * 1000);
                }
                Logger.getLogger("HomeController").log(Level.INFO, "Processing is done");

                //Set attribute addSpinner on 0, spinner is no longer shown
                session.setAttribute("addSpinner", 0);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "result";

        } else {
            //Processing is not started
            model.addAttribute("form", new FormHandler());
            sqliteConnector.setFromPage("result");
            //Set attribute noResultsAvailable on 1, so that the html can show the warning.
            session.setAttribute("noResultsAvailable", 1);

            return "form";
        }

    }

    @PostMapping("/form")
    /**
     * This method handles the submit after the submit button is pressed in the form.
     * @throws IOException when a file can not be opened or does not exist
     * @return String result
     */
    public String formSubmit(
            @RequestParam("cellCountFile") MultipartFile cellCountFile,
            @RequestParam("expressionLevelFile") MultipartFile expressionLevelFile,
            @RequestParam("genotypeDosageFile") MultipartFile genotypeDosageFile,
            @RequestParam("snpToTestFile") MultipartFile snpToTestFile,
            @RequestParam(value = "ad", required = false) String paramAd,
            @RequestParam(value = "m", required = false) String paramM,
            @RequestParam(value = "nn", required = false) String paramNn,
            @Valid FormHandler form,
            HttpSession session) throws IOException {

        if (sqliteConnector.getFromPage().equals("form")) {
            sqliteConnector.startUp();
        } else if (sqliteConnector.getFromPage().equals("reinitialize")) {
            sqliteConnector.reinitialize();
        } else if (sqliteConnector.getFromPage().equals("result")) {
        } else {
            Logger.getLogger("HomeController").log(Level.INFO, "Error from page");
        }

        flagProcessingDone = false;
        //Set the params
        paramAd = "FlagAd";
        paramNn = "FlagNn";

        //Create the connection of the server with the client
        MessageClient.createServerConnection();
        FileCopyist.copyFiles(cellCountFile, expressionLevelFile, genotypeDosageFile, snpToTestFile, paramAd, paramM, paramNn, form.getParamW());

        SessionHandler.setSessionAttributes(session);

        flagProcessingDone = true;
        return "result";
    }

    @RequestMapping("/Error")
    /**
     * This method handles the errors. Shows an error on the url /error.
     */
    public String showError(){
        return "error";
    }


    @GetMapping("/exit")
    /**
     * This method handles the exit when the /exit url is entered. It shuts down the whole springapplication.
     */
    public void exit() {
        shutdownManager.initiateShutdown(1);
        Logger.getLogger("HomeController").log(Level.INFO,  "Shutdown successfully");
    }

}