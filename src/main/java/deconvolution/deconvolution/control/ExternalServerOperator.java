/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution.deconvolution.control;

import deconvolution.DeconvolutionApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
/**
 * This class starts a external server that can perform heavy calculations so the web server is not bothered by it.
 * The external server can receive messages from the web server and can start the deconvolution.jar.
 */
public class ExternalServerOperator {
    HttpSession session;

    @EventListener
    /**
     * This method is called first after running the program because of the eventlistener annotation.
     */
    public void handleContextRefresh(ContextRefreshedEvent event) throws SQLException, ClassNotFoundException {

        Runnable messageServerStarter = new Runnable() {
            @Override
            /**
             * The method run starts a external server that can receive messages from the web server and send messages back
             *  It uses a jar that starts the external server on port 8081.
             *  The communication between the two servers is handled by serversockets.
            */
            public void run() {
                int error = 0;
                final File f = new File(
                        DeconvolutionApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
                String path = f.toString().split("Tool")[0] + "/Tool/";
                String greetingserver = path + "MessageServer/GreetingServer.jar";
                String command = "java -jar " + greetingserver;
                String tmpdir = System.getProperty("java.io.tmpdir");

                try {
                    Process process = Runtime.getRuntime().exec(command, null, new File(tmpdir));

                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

                    // Read the output from the command
                    Logger.getLogger("ExternalServerOperator").log(Level.INFO, "Output of the command:\n");
                    String s = null;

                    while ((s = stdInput.readLine()) != null) {
                        Logger.getLogger("ExternalServerOperator").log(Level.INFO, s);
                        if(s.contains("Exception")){
                            Logger.getLogger("ExternalServerOperator").log(Level.WARNING, "There was an error during executing deconvolution, redirecting...");
                            error = 1;
                            getErrorCode(error, session);

                        }
                    }
                    // read any errors from the attempted command
                    Logger.getLogger("ExternalServerOperator").log(Level.INFO, "Standard error of the command (if any):");

                    while ((s = stdError.readLine()) != null) {
                        Logger.getLogger("ExternalServerOperator").log(Level.INFO, s);
                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(messageServerStarter);
        t.start();
    }


    /**
     * This method is the beginning of errorhandling when the deconvolution program raises an error.
     * Not implemented by the designers of this webapp, a TODO for the creator of deconvolution
     */
    public ModelAndView getErrorCode(int error, HttpSession session) {
        if (error == 1) {
            session.setAttribute("error", 1);
            Logger.getLogger("ExternalServerOperator").log(Level.INFO, "There has been an error!");
            return new ModelAndView(new RedirectView("/Error"));

        }

//            PopupCreator.errorBox("Er is een error ontstaan, probeer opnieuw", "Warning! Error!");
            return new ModelAndView(new RedirectView("/Error"));
    }

}