/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution.deconvolution.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import deconvolution.deconvolution.database.DeconvolutionDao;
import deconvolution.deconvolution.database.sqlite.DataToDbSetter;
import deconvolution.deconvolution.database.sqlite.SqliteConnector;
import deconvolution.deconvolution.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class controls the view of the data using DataTables.
 * The table is created using the pvalues retrieved from the SQLite database.
 * CSV values are retrieved from the database and stored in a json object.
 */
@Controller
public class DataViewController {
    private DeconvolutionDao deconvolutionDao;
    private DataToDbSetter dataToDbSetter;

    private HomeController homeController;

    public static boolean select = false;
    private int records;

    private int startRange;
    private int stopRange;

    private boolean addOke = false;

    public boolean isAddOke() { return addOke; }

    public void setAddOke(boolean addOke) { this.addOke = addOke; }

    public int getStartRange() {
        return startRange;
    }

    public void setStartRange(int startRange) {
        this.startRange = startRange;
    }

    public int getStopRange() {
        return stopRange;
    }

    public void setStopRange(int stopRange) {
        this.stopRange = stopRange;
    }

    public static boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }


    /**
     * Constructor of the DataViewController
     * @param deconvolutionDao
     * @param dataToDbSetter
     * @param homeController
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @Autowired
    public DataViewController(DeconvolutionDao deconvolutionDao, DataToDbSetter dataToDbSetter, HomeController homeController) throws SQLException, ClassNotFoundException {
        this.deconvolutionDao = deconvolutionDao;
        this.dataToDbSetter = dataToDbSetter;
        this.homeController = homeController;
    }

    /**
     * Method to get the max index for the datatable table of the genotypes.
     * @param length
     * @param genotypes
     * @return int toIndex
     */
    public int includeRES(int length, List<Genotype> genotypes) {
        int toIndex = length;
        int searchStart = 0;
        if (toIndex > (genotypes.size() -1)) {
            if (genotypes.size() == 1) {
                Logger.getLogger("DataViewController").log(Level.INFO, "genotype.size = 1");
                toIndex = genotypes.size();
            } else if (genotypes.size() == 0) {
                Logger.getLogger("DataViewController").log(Level.INFO, "geen matches gevonden");
                toIndex = 0;
            } else {
                Logger.getLogger("DataViewController").log(Level.INFO, "genotype.size > 1");
                toIndex = genotypes.size();
            }
        }
        return toIndex;
    }

    /**
     * Method to get the max index for the datatable table of the snps.
     * @param length
     * @param snps
     * @return int toIndex
     */
    public int includeSNP(int length, List<Snp> snps) {

        int toIndex = length;
        int searchStart = 0;
        if (toIndex > (snps.size() -1)) {
            if (snps.size() == 1) {
                Logger.getLogger("DataViewController").log(Level.INFO, "snp.size = 1");
                toIndex = snps.size();
            } else if (snps.size() == 0) {
                Logger.getLogger("DataViewController").log(Level.INFO, "geen matches gevonden");
                toIndex = 0;
            } else {
                Logger.getLogger("DataViewController").log(Level.INFO, "snp.size > 1");
                toIndex = snps.size();
            }
        }

        return toIndex;

    }


    @ResponseBody
    @RequestMapping(value="/showData.do", method= RequestMethod.GET)
    /**
     * This method handles retrieving the csv information from the database and stores it in a json object. The data is the data of the genotypes.
     * Gets the requestparams of the datatable ajax request.
     * @return String json
     */
    public String showData(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam("search[value]") String search,
            @RequestParam("order[0][column]") int order,
            @RequestParam("order[0][dir]") String direction)
            throws SQLException, ClassNotFoundException, JsonProcessingException {

        //Create the database table
        dataToDbSetter.createResultsDbTable();


        //Check if the data is already saved, if true: set boolean addOke on true, if false: add csv data to the database table and set boolean addOke on true
        if (dataToDbSetter.isSavedData()) {
            //data is already setted
            setAddOke(true);
        } else {
            int out = dataToDbSetter.addCsvToTable();
            if (out == 0) {
                //No errors found
                setAddOke(true);
            } else {
                //Error occurring when adding csv to databaseTable
                setAddOke(false);
            }
        }

        if (isAddOke()) {
            //Data is setted

            //Set the data for the datatable
            DataTableResponse dataTableResponse = new DataTableResponse();
            dataTableResponse.setDraw(draw);
            dataTableResponse.setRecordsTotal(deconvolutionDao.getDatabaseLength());
            dataTableResponse.setRecordsFiltered(deconvolutionDao.getDatabaseLength());

            //Get the list with genotypes.
            List<Genotype> genotypes = deconvolutionDao.getGenotypesSorted(isSelect(), order, direction, start, search);

            //Set the data again
            int toIndex;
            toIndex = includeRES(length, genotypes);
            dataTableResponse.setData(genotypes.subList(0, toIndex));
            dataTableResponse.setRecordsFiltered(start + genotypes.size());

            dataTableResponse.setRecordsTotal(deconvolutionDao.getLength());

            //Store the dataTableResponse in a json object
            Gson gson = new Gson();
            String json = gson.toJson(dataTableResponse);
            return json;

        } else {
            //Error
            //TODO raise error?
            return "";
        }

    }

    @ResponseBody
    @RequestMapping(value="/showDataSNP.do", method= RequestMethod.GET)
    /**
     * This method handles retrieving the csv information from the database and stores it in a json object. The data is the data with the snps.
     * Gets the requestparams of the datatable ajax request.
     * @return String json
     */
    public String showDataSNP(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam("search[value]") String search,
            @RequestParam("order[0][column]") int order,
            @RequestParam("order[0][dir]") String direction)
            throws SQLException, ClassNotFoundException, JsonProcessingException {


        if (isAddOke()) {
            //Data is setted

            //Set the data for the datatable
            DataTableResponse dataTableResponse = new DataTableResponse();
            dataTableResponse.setDraw(draw);
            dataTableResponse.setRecordsTotal(deconvolutionDao.getDatabaseLength());
            dataTableResponse.setRecordsFiltered(deconvolutionDao.getDatabaseLength());

            //Get the list with snps.
            List<Snp> snps = deconvolutionDao.getSignificantSNPs(getStartRange(), getStopRange(), order, direction, start, search);

            //Set the data again
            int toIndex;
            toIndex = includeSNP(length, snps);
            dataTableResponse.setDataSNP(snps.subList(0, toIndex));
            dataTableResponse.setRecordsFiltered(start + snps.size());

            dataTableResponse.setRecordsTotal(deconvolutionDao.getLength());

            //Store the dataTableResponse in a json object
            Gson gson = new Gson();
            String json = gson.toJson(dataTableResponse);
            return json;
        } else {
            //Error handling logics.
            return "";
        }
    }

    /**
     * This method handles the button to selecct genotypes on the pvalue.
     * It sets the boolean select dependent of the button "select all" or "select" for only those pvalues that are beneath 0.05.
     * Sets the action button to get an color in the html page, dependent on which button you clicked.
     * @param action String
     * @param model Model
     * @return String page
     */
    @RequestMapping(value="/selectGenotypesOnPvalue.do", method= RequestMethod.GET)
    public String selectGenotypes(@RequestParam String action, Model model, HttpSession session) {
        if (action.equals("select")) {
            //set select boolean on true
            setSelect(true);
        } else if (action.equals("selectAll")) {
            //set select boolean on false
            setSelect(false);
        }
        //Set the attribute action so that the button you clicked in the html page gets an color
        model.addAttribute("action", action);
        SessionHandler.setTabSession(session, "Table" );
        return "result";

    }


    /**
     * Method that handles the range you fill in in the snp tab in the results page.
     * It sets the start and stop range to the values you filled in.
     * @param start int
     * @param stop int
     * @param session HTTPSession
     * @return String page
     */
    @RequestMapping(value="/selectNumberOfCells.do", method = RequestMethod.GET)
    public String selectNumberOfCells(@RequestParam int start, int stop, HttpSession session) {
        //set the start en de stop
        setStartRange(start);
        setStopRange(stop);
        SessionHandler.setTabSession(session, "SNP" );
        return "result";

    }

}
