/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution.deconvolution.database.sqlite;

import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import deconvolution.deconvolution.database.DeconvolutionDao;
import deconvolution.deconvolution.model.ColumnDefiner;
import deconvolution.deconvolution.model.Genotype;
import deconvolution.deconvolution.model.Snp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Component;

/**
 * Class that handles the SQLite database operations
 */
@Component
@EnableAutoConfiguration
public class DeconvolutionDaoSqlite implements DeconvolutionDao {
    @Autowired
    SqliteConnector sqliteConnector;

    public int length;

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * Constructor of the DeconvolutionDaoSqlite class.
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public DeconvolutionDaoSqlite() throws SQLException, ClassNotFoundException {

    }

    @Override
    /**
     * This method returns the the number of results in the FirstResults table
     * @return int number
     */
    public int getDatabaseLength() throws SQLException {
        Statement stmt = this.sqliteConnector.getConnection().createStatement();
        //Create the sql statement
        String sql = "SELECT COUNT(*) FROM FirstResults;";
        //Execute the sql statement and store the outcome in an ResultSet
        ResultSet rs = stmt.executeQuery(sql);
        while(rs.next()) {
            //Return the number of results.
            return rs.getInt(rs.getRow());
        }
        stmt.close();
        rs.close();
        return 0;
    }

    @Override
    /**
     * This method checks if the databasetable is available.
     * @return int
     */
    public int checkDatabaseTableAvailable() {
        try {
            DatabaseMetaData dbm = sqliteConnector.getConnection().getMetaData();
            ResultSet tables = dbm.getTables(null, null, "FirstResults", null);

            if (tables.next()) {
                //Table is available
                return 0;
            }
            else {
                //Table is not available
                return 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    /**
     * This method returns the genotypes in a sorted way dependent of the buttons clicked in the datatable table.
     * @return List Genotype
     */
    public List<Genotype> getGenotypesSorted(boolean select, int columnId, String direction, int start, String searchQuery) {
        List<Genotype> data = new ArrayList<>();
        List<String> celltypes = ColumnDefiner.getCelltypes();
        StringBuilder sb = new StringBuilder();
        int count = 0;

        String column;
        if (columnId == 0) {
            column = "genotype";
        } else {
            //Set column with -1 because of the start of counting on 0.
            column = celltypes.get(columnId-1 );
        }

        try {
            Statement stmt = this.sqliteConnector.getConnection().createStatement();
            String sql;
            int cellsize = celltypes.size();
            //Create sql statement dependent of whether there is an search query and it the select button is pressed or not.
            if (select) {
                //Check if the select button is pressed, start the sql query.
                sb.append("SELECT * FROM FirstResults WHERE ");
                for (String cell : celltypes) {
                    count++;
                    if (count == 1) {
                        //First cell
                        sb.append("(" + cell + " < 0.05 ");
                    } else if (count == cellsize) {
                        //Last cell
                        sb.append("OR " + cell + " < 0.05) ");
                    } else {
                        sb.append("OR " + cell + " < 0.05 ");

                    }
                }
                if (searchQuery.isEmpty()) {
                    //If select is true and there is no search query present.
                    sb.append("order by %s %s limit -1 offset %s;");
                    sql = String.format(sb.toString(), column, direction, start);
                } else {
                    //If select is true and there is an search query present.
                    sb.append("AND genotype LIKE '%%%s%%' order by %s %s limit -1 offset %s;");
                    sql = String.format(sb.toString(), searchQuery, column, direction, start);
                }

            } else {
                if (searchQuery.isEmpty()) {
                    //If select is false and there is no search query present.
                    sql = String.format("SELECT * FROM FirstResults order by %s %s limit -1 offset %s;", column, direction, start);
                } else {
                    //If select is false and there is an search query present.
                    sql = String.format("SELECT * FROM FirstResults WHERE genotype LIKE '%%%s%%' order by %s %s limit -1 offset %s;", searchQuery, column, direction, start);
                }

            }

            //Execute the sql statement
            ResultSet rs = stmt.executeQuery(sql);
            data = this.sqlQueriesToGenotypes(rs);

            stmt.close();
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();

            Logger.getLogger("DeconvolutionDaoSqlLite").log(Level.WARNING,
                    "There is something wrong with the sql statement or the database is locked" );
        }
        return data;
    }

    @Override
    /**
     * This method adds the pvalues to the list of genotypes
     * @return List<Genotype>
     * @throws SQLException when the database is locked.
     */
    public List<Genotype> sqlQueriesToGenotypes(ResultSet rs) throws SQLException {
        List<Genotype> data = new ArrayList<>();
        while(rs.next()) {
            Genotype genotype = new Genotype();
            //Set the genotype name
            genotype.setGenotypeName(rs.getString(1));
            List<String> cells = ColumnDefiner.getCelltypes();
            for (String ct : cells) {
                //Get the pvalues of this cell
                float pValue = rs.getFloat(ct.toLowerCase());
                //Add the pvalues to genotype
                genotype.addPvalue(ct, pValue);
            }
            //Add genotype to the List
            data.add(genotype);
        }
        return data;
    }



    @Override
    /**
     * Method to get the whole table but without the pvalues. It has a x if the pvalue is significant.
     * @param columnId
     * @param direction
     * @param start
     * @param searchQuery
     * @return
     */
    public List<Snp> getSignificantSNPs(int startRange, int stopRange, int columnId, String direction, int start, String searchQuery) {
        List<String> celltypes = ColumnDefiner.getCelltypes();
        StringBuilder caseIfBuilder = new StringBuilder();
        StringBuilder whereBuilder = new StringBuilder();
        String sql = "";

        //Check if Spearman_correlation is in the celltypes list, it will not be in this table
        int cellcount;
        if (celltypes.contains("Spearman_correlation")) {
            cellcount = celltypes.size() -1;
        } else {
            cellcount = celltypes.size();
        }

        //Set the select statement
        String select = "select genotype,";

        //Create the cases for the sql statement
        int counter = 0;
        for (int i = 0; i< cellcount; i++) {
            counter++;
                if (counter == cellcount) {
                    //Last cell
                    caseIfBuilder.append(" (CASE WHEN " + celltypes.get(i) + " < 0.05 THEN 'x'  ELSE '' END) AS " + celltypes.get(i));
                } else if (counter > cellcount) {
                    //cell == spearman
                    //TODO niks doen, pass?
                } else {
                    //Every other cell
                    caseIfBuilder.append(" (CASE WHEN " + celltypes.get(i) + " < 0.05 THEN 'x'  ELSE '' END) AS " + celltypes.get(i) + ",");
                  }
        }
        //Set the caseIf with the stringbuilder caseIfBuilder
        String caseIf = caseIfBuilder.toString();
        //Begin with the from tablename part of the sql statement
        String from = " FROM FirstResults ";

        //Create the where statement of the sql statement
        whereBuilder.append("WHERE ");
        counter = 0;
        for (int i = 0; i< cellcount; i++) {
            counter++;
            if (counter == cellcount) {
                //Last cell
                if(startRange == 0 && stopRange == 0) {
                    whereBuilder.append(" OR " + celltypes.get(i) + " < 0.05)");
                } else {
                    whereBuilder.append(" BETWEEN " + startRange + " AND "+ stopRange + ")");
                }

            } else if (counter > cellcount) {
                //cell == spearman
            } else {
                //Every other cell
                if(startRange == 0 && stopRange == 0) {
                    //Start and stoprange are not filled in
                    if (counter == 1) {
                        whereBuilder.append( "(" + celltypes.get(i) + " < 0.05");
                    } else {
                        whereBuilder.append(" OR " + celltypes.get(i) + " < 0.05");
                    }
                } else {
                    //Start and stoprange are filled in
                    if (counter == 1) {
                        whereBuilder.append("((CASE WHEN "+ celltypes.get(i) + " < 0.05 THEN 1 ELSE 0 END)");
                    } else {
                        whereBuilder.append(" + (CASE WHEN "+ celltypes.get(i) + " < 0.05 THEN 1 ELSE 0 END)");
                    }
                }
            }
        }
        //If there is no search query filled in
        if (!searchQuery.isEmpty()) {
            whereBuilder.append(" AND genotype LIKE '%"+ searchQuery + "%'");
        }
        //Set the where part of the sql statement
        String whereS = whereBuilder.toString();


        String columnOrder;
        if (columnId == 0) {
            columnOrder = "genotype";
        } else {
            //Set columnOrder with -1 because of the start of counting on 0.
            columnOrder = celltypes.get(columnId-1 );
        }
        //Set the orderby part of the sql statement
        String orderby = " ORDER BY " + columnOrder + " " + direction + " LIMIT -1 OFFSET " + start + ";";

        //Paste all sql parts to sql
        sql = select + caseIf + from + whereS + orderby;

        List<Snp> data = new ArrayList<>();
        try {
            Statement stmt = this.sqliteConnector.getConnection().createStatement();
            //Execute the sql statement and store the result in a ResultSet
            ResultSet rs = stmt.executeQuery(sql);
            data = this.sqlQueriesToSNPs(rs);
            stmt.close();
            rs.close();
        } catch (SQLException e) {
            Logger.getLogger("DeconvolutionDaoSqlLite").log(Level.INFO, "e: " + e.getMessage() );
            e.printStackTrace();
        }

        return data;
    }


    @Override
    /**
     * This method adds the values to the list of snps
     * @param rs
     * @return List<Snp>
     * @throws SQLException when the database is locked
     */
    public List<Snp> sqlQueriesToSNPs(ResultSet rs) throws SQLException {
        List<Snp> data = new ArrayList<>();
        List<String> cells = ColumnDefiner.getCelltypes();
        while(rs.next()) {
            Snp snp = new Snp();
            int cellcount;
            //Check if the cell list contains Spearman_correlation, it will not be in this table
            if (cells.contains("Spearman_correlation")) {
                cellcount = cells.size() -1;
            } else {
                cellcount = cells.size();
            }

            //Set the name of the snp
            snp.setSnpName(rs.getString(1));

            for (int i = 0; i < cellcount; i ++) {
                if(!cells.get(i).equals("Spearman_correlation")) {
                    //Store the value of the cell in a string
                    String value = rs.getString(cells.get(i));
                    //Add the value to the snp
                    snp.addCelltype(value);
                }
            }
            //Add the snp to the data list
            data.add(snp);
        }
        return data;
    }
}