/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution.deconvolution.database.sqlite;

import deconvolution.DeconvolutionApplication;
import deconvolution.deconvolution.database.DeconvolutionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class establishes a connection to the embedded sqlite database
 */
@Component
public class SqliteConnector {
    private DataToDbSetter dataToDbSetter;
    private DeconvolutionDao deconvolutionDao;

    private Connection connection;
    private final File deconvolutionApplicationFile;
    private final String path;
    private final String dbUrl;
    private String fromPage;

    public String getFromPage() {
        return fromPage;
    }

    public void setFromPage(String fromPage) {
        this.fromPage = fromPage;
    }

    @Autowired
    @Lazy
    /**
     * This method is the constructor of the sqliteConnector. It sets the variables necessary for the sqlite connection.
     */
    public SqliteConnector(DataToDbSetter dataToDbSetter, DeconvolutionDao deconvolutionDao) {
        this.dataToDbSetter = dataToDbSetter;
        this.deconvolutionDao = deconvolutionDao;
        this.deconvolutionApplicationFile = new File
                (DeconvolutionApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        this.path = deconvolutionApplicationFile.toString().split("Tool")[0] + "/Tool/";
        this.dbUrl = "jdbc:sqlite:" + path + "Database/deconvolutionResults.db";
        createConnection();
    }


    /**
     * This method creates the connection with the database.
     */
    private void createConnection() {
        if (this.dataToDbSetter == null) {
            Logger.getLogger("SqliteConnector").log(Level.INFO, "Error. DataToDbSetter is null");
            throw new IllegalArgumentException("Collaborator cannot be null");
        }
        try {
            this.connection = DriverManager.getConnection(dbUrl);
            Logger.getLogger("SqliteConnector").log(Level.INFO, "Connection created");
        } catch (SQLException ex) {
            throw new RuntimeException("Error creating connection");
        }
    }

    /**
     * This method returns the connection.
     * @return
     */
    public Connection getConnection() {
        return this.connection;
    }


    /**
     * This method handles the first startup of the application. It checks it the database table is available it true: go to reinitialize.
     */
    public void startUp() {
        int out = deconvolutionDao.checkDatabaseTableAvailable();
        if (out == 0) {
            //Table is available
            reinitialize();
        }

    }

    /**
     * This method handles the reinitializing of the program. It deletes the database table and creates a new connection
     */
    public void reinitialize() {
        //Delete table
        this.dataToDbSetter.deleteDatabaseTable("FirstResults");
        this.dataToDbSetter.setSavedData(false);
        //Create connection
        createConnection();
    }

}