package deconvolution.deconvolution.database.sqlite;

import au.com.bytecode.opencsv.CSVReader;
import deconvolution.DeconvolutionApplication;
import deconvolution.deconvolution.model.ColumnDefiner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.sqlite.SQLiteException;
import org.thymeleaf.util.StringUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that handles the SQLIte database write operations.
 * It makes it possible to set the data retrieved from the csv from the deconvolution program to the database.
 * It also deletes the old data when a new result is loaded
 */
@Component
public class DataToDbSetter {

    SqliteConnector sqliteConnector;

    public boolean savedData = false;

    @Autowired
    @Lazy
    public void setSqliteConnector(SqliteConnector sqliteConnector) throws SQLException, ClassNotFoundException {
        this.sqliteConnector = sqliteConnector;
    }

    public boolean isSavedData() {
        return savedData;
    }

    public void setSavedData(boolean savedData) {
        this.savedData = savedData;
    }


    /**
     * This method adds the data of a csv file to the results table in the database.
     * It creates the sql statement and writes the data to the table.
     * @return int.
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public int addCsvToTable()  throws SQLException, ClassNotFoundException {
        //Get the tmp directory of the system.
        String tmpdir = System.getProperty("java.io.tmpdir");
        String csvLocation = tmpdir + "/deconvolutionResults.csv";
        List<String> celltypes = ColumnDefiner.getCelltypes();
        StringBuilder sb = new StringBuilder();
        int aantalCelltypes = celltypes.size();
        int count = 0;

        String valueParams = StringUtils.repeat('?', aantalCelltypes+1).replace("", ",").trim();
        valueParams = valueParams.substring(1, valueParams.length()-1);

        //Begin with the sql statement
        sb.append("INSERT INTO FirstResults (genotype, ");
        for (String cell : celltypes) {
            //Add all values with cells to the statement
            count ++;
            if (count == aantalCelltypes) {
                sb.append(cell + ") values (");
            } else {
                sb.append(cell + ", ");
            }

        }
        String insertQuery = sb + valueParams + ")";
        sqliteConnector.getConnection().setAutoCommit(false);

        //Read the data
        try (CSVReader reader = new CSVReader(new FileReader(csvLocation), ',')) {
            PreparedStatement pstmt = sqliteConnector.getConnection().prepareStatement(insertQuery);
            String[] rowData = null;
            int i = 0;
            while ((rowData = reader.readNext()) != null) {
                if (i != 0) {
                    for (String data : rowData) {
                        String[] columns = Arrays.copyOfRange(data.split("\\s"), 0, (celltypes.size()+1));
                        int queryIndex = 1;
                        for (String col : columns) {
                            pstmt.setString(queryIndex, col);
                            queryIndex++;
                        }
                        //Execute the prepared statement
                        pstmt.executeUpdate();
                    }
                }
                i++;
            }

            Logger.getLogger("DataToDbSetter").log(Level.INFO, "Data is successfully uploaded" );
            setSavedData(true);
            pstmt.close();
            sqliteConnector.getConnection().commit();
            sqliteConnector.getConnection().setAutoCommit(true);
        } catch (SQLiteException e) {
            Logger.getLogger("DataToDbSetter").log(Level.WARNING, "SQLITE exception: " + e.getMessage() );
            deleteDatabaseFile();
        } catch (IOException io) {
            Logger.getLogger("DataToDbSetter").log(Level.WARNING, "IOException: " + io.getMessage() );
            return 1;
        }
        return 0;
    }


    /**
     * This method deletes the database file.
     * @return int
     */
    public int deleteDatabaseFile() {
        try {
            //First close the database so the database won't be locked
            sqliteConnector.getConnection().close();

            File file = new File(DeconvolutionApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            String path = file.toString().split("Tool")[0] + "/Tool/Database/deconvolutionResults.db";

            File removeFile = new File(path);

            if(removeFile.delete()) {
                Logger.getLogger("DataToDbSetter").log(Level.INFO, removeFile.getName() + " is deleted");
            } else {
                Logger.getLogger("DataToDbSetter").log(Level.INFO, "Delete operation on " + removeFile.getName() + " failed");
            }
        } catch(Exception e){
            e.printStackTrace();
            return 1;
        }


        return 0;
    }

    /**
     * This method deletes the database table when new data should be uploaded
     * @param tablename
     * @return int
     */
    public int deleteDatabaseTable(String tablename) {
        try {
            sqliteConnector.getConnection().setAutoCommit(false);
            Statement stmt = sqliteConnector.getConnection().createStatement();
            //Setup the sql statement
            String sql = "DROP TABLE IF EXISTS " + "'" + tablename + "'" + ";";
            //Execute the statement
            stmt.executeUpdate(sql);
            Logger.getLogger("DataToDbSetter").log(Level.INFO, "Deleted table " + tablename);
            stmt.close();
            sqliteConnector.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.getLogger("DataToDbSetter").log(Level.WARNING,
                    "There is something wrong with the sql statement or the database is locked" );
            //The database is locked, try to delete the databaseFile.
            deleteDatabaseFile();
            return 1;
        }
        return 0;
    }

    /**
     * This method creates the database table when it does not exist
     * @return int
     * @throws SQLException
     */
    public int createResultsDbTable() throws SQLException {
        try {
            Statement stmt = sqliteConnector.getConnection().createStatement();

            List<String> celltypes = ColumnDefiner.getCelltypes();
            StringBuilder sb = new StringBuilder();
            int aantalCelltypes = celltypes.size();
            int count = 0;

            //Create the sql statement
            sb.append("CREATE TABLE FirstResults (genotype VARCHAR(20) PRIMARY KEY, ");
            for (String cell : celltypes) {
                count ++;
                if (count == aantalCelltypes) {
                    sb.append(cell + " FLOAT)");
                } else {
                    sb.append(cell + " FLOAT, ");
                }
            }
            String sql = sb.toString();
            //Execute the sql statement
            stmt.executeUpdate(sql);
            Logger.getLogger("DataToDbSetter").log(Level.INFO,
                    "Created table FirstResults");

            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.getLogger("DataToDbSetter").log(Level.WARNING,
                    "There is something wrong with the sql statement or the database is locked" );
            return 1;
        }
        return 0;
    }
}

