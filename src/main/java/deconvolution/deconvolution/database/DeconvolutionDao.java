package deconvolution.deconvolution.database;

import deconvolution.deconvolution.model.Genotype;
import deconvolution.deconvolution.model.Snp;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This is the interface for the Database operator.
 */
@EnableAutoConfiguration
public interface DeconvolutionDao {

    void setLength(int length);
    int getLength();

    /**
     * This method returns the the number of results in the FirstResults table
     * @return int number
     */
    int getDatabaseLength() throws SQLException;

    /**
     * This method checks if the databasetable is available.
     * @return int
     */
    int checkDatabaseTableAvailable();


    /**
     * This method returns the genotypes in a sorted way dependent of the buttons clicked in the datatable table.
     * @return List Genotype
     */
    List<Genotype> getGenotypesSorted(boolean select, int columnId, String direction, int start, String searchQuery);


    /**
     * This method adds the pvalues to the list of genotypes
     * @return List<Genotype>
     * @throws SQLException when the database is locked.
     */
    List<Genotype> sqlQueriesToGenotypes(ResultSet rs) throws SQLException;


    /**
     * Method to get the whole table but without the pvalues. It has a x if the pvalue is significant.
     * @param columnId
     * @param direction
     * @param start
     * @param searchQuery
     * @return
     */
    List<Snp> getSignificantSNPs(int startRange, int stopRange, int columnId, String direction, int start, String searchQuery);

    /**
     * This method adds the values to the list of snps
     * @param rs
     * @return List<Snp>
     * @throws SQLException when the database is locked
     */
    List<Snp> sqlQueriesToSNPs(ResultSet rs) throws SQLException;

}
