/*
This is a web application for the deconvolution program
Commissioned by UMCG and Hanze Hogeschool Groningen

(c) Maaike Brummer and Iris Gorter 2017

 */

package deconvolution;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@SpringBootApplication
public class
DeconvolutionApplication {
	/**
	 * This is the main method of the deconvolution web application
	 * This method starts the embedded tomcat on port 8080.
	 */
	public static void main(String[] args) throws URISyntaxException {
		SpringApplication.run(DeconvolutionApplication.class, args);
		String url = "http://localhost:8080";

		if (Desktop.isDesktopSupported()) {
			//Start the default browser on Windows
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			// Start the default browser on Ubuntu and macOS
			Runtime runtime = Runtime.getRuntime();
			try {
				runtime.exec("/usr/bin/firefox -new-window " + url);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}