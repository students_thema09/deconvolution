package deconvolution.deconvolution.server;

import deconvolution.deconvolution.control.HomeController;
import deconvolution.deconvolution.model.CellType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileNotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageClientTest {

    /**
     * Test method sunny day. Files exist and are not empty.
     * @throws FileNotFoundException
     * @throws CellType.EmptyFileException
     */
    @Test
    public void sendMessageTest() throws FileNotFoundException, CellType.EmptyFileException {
        HomeController hc = new HomeController();
        MessageClient messageClient = new MessageClient(hc);
        File cellCountFile = new File("/home/iris/tmp/simulatedCellcount.txt");
        File expressionlevelFile = new File("/home/iris/tmp/simulatedExpressionData.txt");
        File genotypeDosageFile = new File("/home/iris/tmp/simulatedGenotypeData.txt");
        File snpFile = new File("home/iris/tmp/simulatedGeneSNPpair.txt");
        String tmpdir = "home/iris/tmp/";

        messageClient.sendMessage(cellCountFile, expressionlevelFile, genotypeDosageFile, snpFile, "", "", "", "", tmpdir );
    }

    /**
     * Test method with the empty file cellcounts.
     * @throws FileNotFoundException
     * @throws CellType.EmptyFileException
     */
    @Test(expected = CellType.EmptyFileException.class)
    public void sendMessageTestEmptyFile() throws FileNotFoundException, CellType.EmptyFileException {
        HomeController hc = new HomeController();
        MessageClient messageClient = new MessageClient(hc);
        File cellCountFile = new File("/home/iris/tmp/simulatedCellcount_empty.txt");
        File expressionlevelFile = new File("/home/iris/tmp/simulatedExpressionData.txt");
        File genotypeDosageFile = new File("/home/iris/tmp/simulatedGenotypeData.txt");
        File snpFile = new File("home/iris/tmp/simulatedGeneSNPpair.txt");
        String param = "";
        String tmpdir = "home/iris/tmp/";

        messageClient.sendMessage(cellCountFile, expressionlevelFile, genotypeDosageFile, snpFile, "", "", "", "", tmpdir );

    }

    /**
     * Test method with file not found on the path.
     * @throws FileNotFoundException
     * @throws CellType.EmptyFileException
     */
    @Test(expected = FileNotFoundException.class)
    public void sendMessageTestFileNotFound() throws FileNotFoundException, CellType.EmptyFileException {
        HomeController hc = new HomeController();
        MessageClient messageClient = new MessageClient(hc);
        File cellCountFile = new File("/home/iris/bestaatniet/simulatedCellcount.txt");
        File expressionlevelFile = new File("/home/iris/bestaatniet/simulatedExpressionData.txt");
        File genotypeDosageFile = new File("/home/iris/bestaatniet/simulatedGenotypeData.txt");
        File snpFile = new File("home/iris/bestaatniet/simulatedGeneSNPpair.txt");
        String param = "";
        String tmpdir = "home/iris/bestaatniet/";

        messageClient.sendMessage(cellCountFile, expressionlevelFile, genotypeDosageFile, snpFile, "", "", "", "", tmpdir );

    }
}
